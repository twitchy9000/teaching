import express from 'express';

const app = express();

const startTime = Date.now();
app.get('/health', (req, res) => {
  console.log('Health check');
  res.json({
    uptime: Date.now() - startTime,
  });
});

export default app;
