FROM node:12 as base

WORKDIR /app

COPY package*.json ./

RUN npm i --production

RUN mkdir production/

RUN mv node_modules/ production/node_modules

RUN npm i

COPY . .

RUN npm run compile

CMD ["npm", "run", "dev:watch"]
